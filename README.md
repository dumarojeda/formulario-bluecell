# Formulario Bluecell Plugin

## Descripción

El plugin "Formulario Bluecell" es una extensión para WordPress que agrega un formulario personalizado debajo del contenido de las entradas individuales (solo en las páginas de tipo "single"). Cuando el plugin se activa, crea una base de datos para almacenar las envíos de formularios. Al desactivar el plugin, esta base de datos se borrara. Los datos del formulario son enviados mediante AJAX y se muestran en un panel de administración específico para el usuario administrador de WordPress.

## Estructura de Directorios

```
├── admin
│   ├── css
│   │   ├── dashboard-bluecell.css
│   ├── js
│   │   ├── dashboard-bluecell.js
├── includes
│   ├── controllers
│   │   ├── options-page.php
│   │   ├── form.php
│   │   ├── setup.php
│   │   ├── smtp.php
│   ├── models
│   │   ├── submission.php
│   ├── views
│   │   ├── admin-email-notification.php
│   │   ├── admin-options-template.php
│   │   ├── admin-submissions-template.php
│   │   ├── form-template.php
├── public
│   ├── css
│   │   ├── formulario-bluecell.css
│   ├── js
│   │   ├── formulario-bluecell.js
├── formulario-bluecell.php
├── index.php
└── .gitignore
```

## Estructura del Plugin

El plugin consta de varias carpetas y archivos, organizados de la siguiente manera:

### 1. admin

Contiene los archivos CSS y JavaScript necesarios para el panel de administración del plugin.

- **css/dashboard-bluecell.css**: Este archivo contiene los estilos personalizados para la tabla de registros del formulario en el panel de administración.

- **js/dashboard-bluecell.js**: Este archivo contiene scripts JavaScript personalizados para el panel de administración del plugin.

### 2. includes

Esta carpeta contiene los archivos que manejan la lógica principal del plugin.

#### 2.1. controllers

Contiene los controladores que gestionan la interacción entre los modelos y las vistas del plugin.

- **options-page.php**: Controlador que maneja la lógica para mostrar los datos registrados del formulario y opciones de configuración del plugin como envió de correos.

- **form.php**: Controlador que maneja la lógica del formulario y el envío de datos.

- **setup.php**: Controlador que registra los assets requeridos tanto para el administrador de WordPress como para el sitio.

- **smtp.php**: Controlador que configura el envío de correos electrónicos utilizando SMTP.

#### 2.2. models

Contiene los modelos que definen la estructura de datos y la interacción con la base de datos.

- **submission.php**: Modelo que define la estructura de los datos de envío de registros del formulario y la interacción con la base de datos.

#### 2.3. views

Contiene las plantillas de las vistas del plugin.

- **admin-email-notification.php**: Plantilla para envió de correo electrónico de notificación de un nuevo registro del formulario.

- **admin-options-template.php**: Plantilla que muestra las opciones de configuración del plugin en el panel de administración.

- **admin-submissions-template.php**: Plantilla que muestra los registros del formulario en el panel de administración.

- **form-template.php**: Plantilla que muestra el formulario en las páginas de tipo "single".

### 3. public

Contiene los archivos CSS y JavaScript necesarios para el lado público del sitio web.

- **css/formulario-bluecell.css**: Este archivo contiene estilos personalizados para el formulario en las páginas de tipo "single".

- **js/formulario-bluecell.js**: Este archivo contiene scripts JavaScript personalizados para el formulario.

### 4. formulario-bluecell.php

Este es el archivo principal del plugin. Aquí se definen incluyen los controladores y modelos a usar, se crea/elimina la base de datos al activar/desactivar el plugin y se definen las acciones y filtros del plugin.

### 5. index.php

Este archivo evita el acceso directo a la estructura de directorios del plugin.

## Uso

1. Instala y activa el plugin "Formulario Bluecell" desde la sección de plugins de WordPress.

2. Una vez activado, el plugin creará automáticamente la base de datos necesaria para almacenar los envíos del formulario.

3. En las páginas de tipo "single", el formulario se mostrará automáticamente debajo del contenido de cada entrada.

4. Los datos del formulario serán enviados mediante AJAX y almacenados en la base de datos.

5. En el administrador de WordPress, el plugin mostrará un menú llamado "Formulario Bluecell" este mostraba dos submenus:

   1. Registros: Cargará una tabla con los datos registrados del formulario.

   2. Opciones: Aquí puedes configurar las opciones de envío de correo.

### Gracias.
