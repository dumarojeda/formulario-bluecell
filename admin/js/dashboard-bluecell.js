jQuery(document).ready(function () {
  /** */
  jQuery('#submissions-table').DataTable({
    dom: 'Bfrtip',
    buttons: ['copyHtml5', 'excelHtml5', 'csvHtml5', 'pdfHtml5'],
  });

  jQuery('[data-slug="formulario-blucell"] a').on('click', function (event) {
    event.preventDefault();
    var urlRedirect = document
      .querySelector('[data-slug="formulario-blucell"] a')
      .getAttribute('href');
    if (
      confirm(
        '¿Estas seguro de desactivar el plugin, todos los datos se borraran?'
      )
    ) {
      window.location.href = urlRedirect;
    }
  });
});
