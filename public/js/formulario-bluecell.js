const sendDataViaAjax = (() => {
  /** Initializes DOM Constant for Cache. */
  const $contactForm = jQuery('form.contact-form');
  const $messages = jQuery('.wrapper-form #messages');
  const $nameField = jQuery('input[name="name"]');
  const $emailField = jQuery('input[name="email"]');
  const $phoneField = jQuery('input[name="phone"]');
  const $messageField = jQuery('textarea[name="message"]');
  const $subjectField = jQuery('input[name="subject"]');
  const $policiesConditionsField = jQuery('input[name="policies_conditions"]');

  /** Initializes DOM Constant for Cache. */
  $nameField
    .add($emailField)
    .add($phoneField)
    .add($messageField)
    .add($subjectField)
    .add($policiesConditionsField)
    .on('focus', function () {
      const $currentField = jQuery(this);
      const fieldName = $currentField.attr('name');
      $messages.find(`[data-error-for="${fieldName}"]`).remove();
    });

  /** Sending form via Ajax. */
  const events = () => {
    $contactForm.submit((event) => {
      event.preventDefault();

      // Clear previous error messages
      $messages.empty();
      $messages.html();
      $messages.removeClass();

      // Validate fields
      let isValid = true;

      if ($nameField.val().trim() === '') {
        displayErrorMessage('Por favor escribe tu Nombre.', 'name');
        isValid = false;
      }

      if (!isValidEmail($emailField.val())) {
        displayErrorMessage('Por favor escribe un Email valido.', 'email');
        isValid = false;
      }

      if ($phoneField.val().trim() === '') {
        displayErrorMessage(
          'Por favor escribe tu numero de Tel&eacute;fono.',
          'phone'
        );
        isValid = false;
      }

      if ($messageField.val().trim() === '') {
        displayErrorMessage('por favor escribe tu mensaje.', 'message');
        isValid = false;
      }

      if ($subjectField.val().trim() === '') {
        displayErrorMessage('Por favor escribe tu asunto.', 'subject');
        isValid = false;
      }

      if (!$policiesConditionsField.prop('checked')) {
        displayErrorMessage(
          'Por favor acepta las Pol&iacute;ticas y Condiciones.',
          'policies_conditions'
        );
        isValid = false;
      }

      if (isValid) {
        jQuery.ajax({
          type: 'POST',
          url: $contactForm.attr('action'),
          data: $contactForm.serialize(),
          success: function (res) {
            $messages.html(res.data.message).addClass('success').fadeIn();

            $contactForm[0].reset();
          },
          error: function (res) {
            $messages.addClass('error').html(res.data.message).fadeIn();
          },
        });
      }
    });
  };

  const isValidEmail = function (email) {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    return emailRegex.test(email);
  };

  const displayErrorMessage = function (message, fieldName) {
    $messages
      .addClass('error')
      .append(`<div data-error-for="${fieldName}">${message}</div>`)
      .fadeIn();
  };

  return {
    init: () => {
      events();
    },
  };
})();

const pluginFormularioBluecell = (() => ({
  init: () => {
    sendDataViaAjax.init();
  },
}))();

pluginFormularioBluecell.init();
