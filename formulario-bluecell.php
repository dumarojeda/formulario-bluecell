<?php

/**
 * Plugin Name:   Formulario Blucell
 * Plugin URI:    https://https://github.com/dumarojeda/formulario-bluecell
 * Description:   This is a plugin with a custom form to will be added after the_content() in single.
 * Author:        Dumar Ojeda
 * Author URI:    https://gitlab.com/dumarojeda/
 * Version:       1.0.0
 * Text Domain:   formulario_blucell
 */


/* Exit if accessed directly. */

if (!defined('ABSPATH')) {
    exit;
}

use FormularioBluecell\Models\Submission;

/* Plugin constants. */

define('PLUGIN_PATH', plugin_dir_path(__FILE__));
define('PLUGIN_URL', plugin_dir_url(__FILE__));
define('PLUGIN_VERSION', '1.0.0');

/* Load Controllers. */
require PLUGIN_PATH . 'includes/controllers/setup.php';
require PLUGIN_PATH . 'includes/controllers/smtp.php';
require PLUGIN_PATH . 'includes/controllers/form.php';
require PLUGIN_PATH . 'includes/controllers/options-page.php';

/* Load Models. */
require PLUGIN_PATH . 'includes/models/submission.php';

/* Initialize Controllers. */
new FormularioBluecell\Controllers\SetupController;
new FormularioBluecell\Controllers\SMTPController;
new FormularioBluecell\Controllers\FormController;
new FormularioBluecell\Controllers\OptionsPageController;

/* Register and deactivation. */
register_activation_hook(__FILE__, 'activate');
register_deactivation_hook(__FILE__, 'deactivate');

function activate()
{
    Submission::create_table();
}

function deactivate()
{
    Submission::delete_table();
}
