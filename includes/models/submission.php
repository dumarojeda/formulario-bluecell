<?php

namespace FormularioBluecell\Models;

/**
 * Submission Model.
 */
class Submission
{
    /**
     * Create table in database.
     */
    public static function create_table()
    {
        global $wpdb;

        $charset_collate = $wpdb->get_charset_collate();
        $table_name = $wpdb->prefix . 'bluecell_formulario';

        $sql = "CREATE TABLE `$table_name` (";
        $sql .= " `id` int(11) NOT NULL AUTO_INCREMENT, ";
        $sql .= " `name` varchar(100) NOT NULL, ";
        $sql .= " `email` varchar(50) NOT NULL, ";
        $sql .= " `phone` varchar(30) NOT NULL, ";
        $sql .= " `message` longtext NOT NULL, ";
        $sql .= " `subject` varchar(50), ";
        $sql .= " `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,";
        $sql .= " PRIMARY KEY (`id`) ";
        $sql .= ") $charset_collate;";

        if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
            dbDelta($sql);
        }
    }

    /**
     * Delete table in database.
     */
    public static function delete_table()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . 'bluecell_formulario';
        $sql = "DROP TABLE IF EXISTS $table_name";
        $wpdb->query($sql);
    }

    /**
     * Insert a new form submission record into the database.
     */
    public static function insert_submission($name, $email, $phone, $message, $subject)
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "bluecell_formulario";

        $now = current_time('mysql');

        $wpdb->insert($table_name, [
            "name" => $name,
            "email" => $email,
            "phone" => $phone,
            "message" => $message,
            "subject" => $subject,
            "created_at" => $now,
        ]);
    }

    /**
     * Get all submissions from the database.
     */
    public static function get_submissions()
    {
        global $wpdb;
        $table_name = $wpdb->prefix . "bluecell_formulario";

        $submissions = $wpdb->get_results("SELECT * FROM $table_name");
        return $submissions;
    }
}
