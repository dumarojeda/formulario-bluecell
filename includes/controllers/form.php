<?php

namespace FormularioBluecell\Controllers;

use FormularioBluecell\Models\Submission;
use FormularioBluecell\Controllers\SMTPController;

/**
 * Form Controller.
 * 
 */
class FormController
{
    public function __construct()
    {
        add_filter('the_content', [$this, 'show_contact_form']);
        add_action('wp_ajax_nopriv_ajax_bluecell_form', [$this, 'ajax_bluecell_form']);
        add_action('wp_ajax_ajax_bluecell_form', [$this, 'ajax_bluecell_form']);
    }

    /**
     * Callback function to display the contact form below the content of single posts.
     */
    public function show_contact_form($content)
    {
        if (is_single()) {
            $form_html = '';

            ob_start();
            require PLUGIN_PATH . 'includes/views/form-template.php';
            $form_html = ob_get_clean();

            $content .= $form_html;
        }

        return $content;
    }

    /**
     * AJAX callback for handling form submissions.
     */
    public function ajax_bluecell_form()
    {
        if (!isset($_POST['bluecell_form_nonce']) || !wp_verify_nonce($_POST['bluecell_form_nonce'], 'wp_nonce_form')) {
            wp_send_json_error(['message' => __('El campo nonce No fue verificado, refresca la pagina e intenalo de nuevo.')]);
        }

        $name = sanitize_text_field($_POST['name']);
        $email = sanitize_email($_POST['email']);
        $phone = sanitize_text_field($_POST['phone']);
        $message = sanitize_textarea_field($_POST['message']);
        $subject = sanitize_text_field($_POST['subject']);

        Submission::insert_submission($name, $email, $phone, $message, $subject);

        $state_mail = SMTPController::send_email(
            $subject,
            PLUGIN_PATH . 'includes/views/admin-email-notification.php',
            [
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'message' => $message,
            ],
        );

        wp_send_json_success([
            'message' => __('El mensaje fue enviado con exito'),
            'state_mail' => $state_mail
        ]);
    }
}
