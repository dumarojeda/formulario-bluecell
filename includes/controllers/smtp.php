<?php

namespace FormularioBluecell\Controllers;

/**
 * SMTP Controller.
 * 
 */
class SMTPController
{
    public function __construct()
    {
        add_action('phpmailer_init', [$this, 'phpmailer_init']);
        add_filter('wp_mail_content_type', [$this, 'wp_mail_content_type']);
        add_filter('wp_mail_from', [$this, 'wp_mail_from']);
        add_filter('wp_mail_from_name', [$this, 'wp_mail_from_name']);
    }

    /**
     * Callback function to set the mail content type to HTML.
     */
    public function wp_mail_content_type()
    {
        return 'text/html';
    }

    /**
     * Callback function to set the 'From' email address for the outgoing email.
     */
    public function wp_mail_from()
    {
        return get_option('smtp_from');
    }

    /**
     * Callback function to set the 'From' name in the email.
     */
    public function wp_mail_from_name()
    {
        return get_option('smtp_from_name');
    }

    /**
     * Callback function to initialize PHPMailer and set SMTP configuration.
     */
    public function phpmailer_init($phpmailer)
    {
        $phpmailer->isSMTP();
        $phpmailer->SMTPAuth = get_option('smtp_auth');
        $phpmailer->From = get_option('smtp_from');
        $phpmailer->FromName = get_option('smtp_from_name');
        $phpmailer->Host = get_option('smtp_host');
        $phpmailer->Password = get_option('smtp_password');
        $phpmailer->Port = get_option('smtp_port');
        $phpmailer->SMTPSecure = get_option('smtp_secure');
        $phpmailer->Username = get_option('smtp_username');
    }

    /**
     * Custom function to send an email using WordPress wp_mail function.
     */
    public static function send_email($subject, $template, $data = [])
    {
        $from = get_option('smtp_from');
        $sender = 'From: ' . get_option('name') . ' <' . $from . '>' . "\r\n";
        $headers[] = 'MIME-Version: 1.0' . "\r\n";
        $headers[] = 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers[] = "X-Mailer: PHP \r\n";
        $headers[] = $sender;
        ob_start();
        $data = $data;
        require($template);
        $email_content = ob_get_clean();

        $emails_to_send = get_option('email_to_send');

        $to = $emails_to_send ? explode(",", $emails_to_send) : get_bloginfo('admin_email');

        return wp_mail($to, $subject, $email_content, $headers);
    }
}
