<?php

namespace FormularioBluecell\Controllers;

use FormularioBluecell\Models\Submissions;

/**
 * Setup Controller.
 * 
 */
class SetupController
{
    public function __construct()
    {
        add_action('wp_enqueue_scripts', [$this, 'public_enqueue_assets']);
        add_action('admin_enqueue_scripts', [$this, 'admin_enqueue_assets']);
    }

    /**
     * Register public assets.
     */
    public function public_enqueue_assets()
    {
        wp_enqueue_style(
            'formulario-bluecell-css',
            PLUGIN_URL . 'public/css/formulario-bluecell.css',
            [],
            PLUGIN_VERSION
        );

        wp_enqueue_script(
            'formulario-bluecell-js',
            PLUGIN_URL . 'public/js/formulario-bluecell.js',
            ['jquery'],
            PLUGIN_VERSION,
            true
        );

        wp_localize_script('formulario-bluecell-js', 'bluecellForm', ['api_url' => admin_url('admin-ajax.php')]);
    }

    /**
     * Register admin assets.
     */
    public function admin_enqueue_assets()
    {
        wp_enqueue_style(
            'datatables-css',
            'https://cdn.datatables.net/v/dt/dt-1.13.5/rr-1.4.1/datatables.min.css',
            [],
            PLUGIN_VERSION
        );

        wp_enqueue_script(
            'datatables-js',
            'https://cdn.datatables.net/v/dt/dt-1.13.5/rr-1.4.1/datatables.min.js',
            ['jquery'],
            PLUGIN_VERSION,
            true
        );

        wp_enqueue_style(
            'dashboard-bluecell-css',
            PLUGIN_URL . 'admin/css/dashboard-bluecell.css',
            [],
            PLUGIN_VERSION
        );

        wp_enqueue_script(
            'dashboard-bluecell-js',
            PLUGIN_URL . 'admin/js/dashboard-bluecell.js',
            ['jquery'],
            PLUGIN_VERSION,
            true
        );
    }
}
