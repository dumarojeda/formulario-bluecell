<?php

namespace FormularioBluecell\Controllers;

use FormularioBluecell\Models\Submission;

/**
 * Options Page Controller.
 * 
 */
class OptionsPageController
{
    public function __construct()
    {
        add_action('admin_menu', [$this, 'add_options_page']);
        add_action('admin_init', [$this, 'options_custom_settings']);
        add_filter('sanitize_option_smtp_auth', [$this, 'validate_smtp_fields'], 1, 2);
    }

    /**
     * Callback function to add the options page to the admin menu.
     */
    public function add_options_page()
    {
        add_menu_page(
            'Formulario Bluecell Options',
            'Formulario Bluecell',
            'manage_options',
            'submissions_option_page',
            [$this, 'template_submissions'],
            'dashicons-media-text',
            4
        );

        add_submenu_page(
            'submissions_option_page',
            'Registros Options',
            'Registros',
            'manage_options',
            'submissions_option_page',
            [$this, 'template_submissions']
        );
        add_submenu_page(
            'submissions_option_page',
            'General Options',
            'Opciones',
            'manage_options',
            'general_options_page',
            [$this, 'template_options']
        );
    }

    /**
     * Callback function to display the submissions template on sub-menu page.
     */
    public function template_submissions()
    {
        $submissions = Submission::get_submissions();

        require PLUGIN_PATH . 'includes/views/admin-submissions-template.php';
    }

    /**
     * Callback function to display the options template on the sub-menu page.
     */
    public function template_options()
    {
        require PLUGIN_PATH . 'includes/views/admin-options-template.php';
    }

    /**
     * Callback function to register custom settings and fields for the options page.
     */
    public function options_custom_settings()
    {
        /** Create field's section. */
        add_settings_section(
            'smtp_settings_section',
            'Activar envio de correos',
            [$this, 'show_info_section'],
            'general_options_page'
        );

        /** Add SMTP fields. */
        add_settings_field(
            'smtp_auth',
            'SMTP Auth',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_auth',
                'type_field' => 'radio',
            ]
        );

        add_settings_field(
            'smtp_host',
            'SMTP Host',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_host',
                'type_field' => 'text',
                'placeholder_field' => 'smtp.bluecell.es',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'smtp_port',
            'SMTP Port',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_port',
                'type_field' => 'text',
                'placeholder_field' => '25',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'smtp_secure',
            'SMTP Secure',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_secure',
                'type_field' => 'text',
                'placeholder_field' => 'tls',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'smtp_from',
            'SMTP From',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_from',
                'type_field' => 'text',
                'placeholder_field' => 'example@bluecell.es',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'smtp_from_name',
            'SMTP From Name',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_from_name',
                'type_field' => 'text',
                'placeholder_field' => 'Bluecell',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'smtp_username',
            'SMTP Username',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_username',
                'type_field' => 'text',
                'placeholder_field' => 'sender@bluecell.es',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'smtp_password',
            'SMTP Password',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'smtp_password',
                'type_field' => 'text',
                'placeholder_field' => 'Password',
                'custom_message' => ''
            ]
        );

        add_settings_field(
            'email_to_send',
            'Email para enviar notificaciones',
            [$this, 'show_field'],
            'general_options_page',
            'smtp_settings_section',
            [
                'name_field' => 'email_to_send',
                'type_field' => 'text',
                'placeholder_field' => 'admin@bluecell.es',
                'custom_message' => 'Puedes ingresar varios y separarlos por coma, si dejas este campo vacio se tomara el mail del administrador.'
            ]
        );


        register_setting('general_options_page', 'smtp_auth');
        register_setting('general_options_page', 'smtp_host',);
        register_setting('general_options_page', 'smtp_port');
        register_setting('general_options_page', 'smtp_secure');
        register_setting('general_options_page', 'smtp_from');
        register_setting('general_options_page', 'smtp_from_name');
        register_setting('general_options_page', 'smtp_username');
        register_setting('general_options_page', 'smtp_password');
        register_setting('general_options_page', 'email_to_send');
    }

    /**
     * Callback function to validate the SMTP fields.
     */
    public function validate_smtp_fields($input, $option)
    {
        $smtp_auth = filter_var($input, FILTER_VALIDATE_BOOLEAN);

        if ($smtp_auth) {

            $smtp_host = trim(get_option('smtp_host', ''));
            $smtp_port = trim(get_option('smtp_port', ''));
            $smtp_secure = trim(get_option('smtp_secure', ''));
            $smtp_from = trim(get_option('smtp_from', ''));
            $smtp_from_name = trim(get_option('smtp_from_name', ''));
            $smtp_username = trim(get_option('smtp_username', ''));
            $smtp_password = trim(get_option('smtp_password', ''));
            $email_to_send = trim(get_option('email_to_send', ''));

            if (empty($smtp_host) || empty($smtp_port) || empty($smtp_secure) || empty($smtp_from) || empty($smtp_from_name) || empty($smtp_username) || empty($smtp_password)) {
                add_settings_error(
                    'general_options_page',
                    'empty_fields',
                    'Por favor, complete todos los campos SMTP.',
                    'error'
                );
            }

            if (!empty($email_to_send) && !is_email($email_to_send)) {

                add_settings_error(
                    'email_to_send',
                    'invalid_email',
                    'Por favor, ingrese un correo electrónico válido.',
                    'error'
                );
            }
        }

        return $input;
    }

    /**
     * Callback function to display the description section for SMTP.
     */
    public function show_info_section()
    {
        echo __('Opcion de envio de correo para cada registro.');
    }

    /**
     * Callback function to display the fields'.
     */
    public function show_field($args)
    {
        $option = get_option($args['name_field']);

        $html = '';

        if ($args['type_field'] === 'radio') {
            $isTrue = filter_var($option, FILTER_VALIDATE_BOOLEAN);

            $html .= '<input type="radio" id="' . $args['name_field'] . '_true" name="' . $args['name_field'] . '" value="1" ' . checked($isTrue, true, false) . ' />' . __('Activado');
            $html .= '&nbsp;';
            $html .= '<input type="radio" id="' . $args['name_field'] . '_false" name="' . $args['name_field'] . '" value="0" ' . checked(!$isTrue, true, false) . ' />' . __('Desactivado');
        }

        if ($args['type_field'] === 'text') {
            $html .= '<input type="text" id="' . $args['name_field'] . '" name="' . $args['name_field'] . '" value="' . $option . '" placeholder="' . $args['placeholder_field'] . '" />';
            if ($args['custom_message']) {
                $html .= '<p>' . $args['custom_message'] . '</p>';
            }
        }

        echo $html;
    }
}
