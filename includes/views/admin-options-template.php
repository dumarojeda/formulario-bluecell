<div class="wrap">
    <h1 class="wp-heading-inline">Opciones de Formulario Bluecell</h1>
    <?php settings_errors(); ?>
    <form method="post" action="options.php">
        <?php settings_fields('general_options_page'); ?>
        <?php do_settings_sections('general_options_page'); ?>
        <?php submit_button(); ?>
    </form>
</div>