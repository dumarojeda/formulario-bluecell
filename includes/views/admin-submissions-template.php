<div class="wrap">
    <h1 class="wp-heading-inline">Formulario Bluecell</h1>
    <table id="submissions-table" class="display" style="width:100%">
        <thead>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Tel&eacute;fono') ?></th>
                <th><?= __('Mensaje') ?></th>
                <th><?= __('Asunto') ?></th>
                <th><?= __('Fecha') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($submissions as $submission) { ?>
                <tr>
                    <td><?= $submission->id; ?></td>
                    <td><?= $submission->name; ?></td>
                    <td><?= $submission->email; ?></td>
                    <td><?= $submission->phone; ?></td>
                    <td><?= $submission->message; ?></td>
                    <td><?= $submission->subject; ?></td>
                    <td><?= $submission->created_at; ?></td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th><?= __('Id') ?></th>
                <th><?= __('Nombre') ?></th>
                <th><?= __('Email') ?></th>
                <th><?= __('Tel&eacute;fono') ?></th>
                <th><?= __('Mensaje') ?></th>
                <th><?= __('Asunto') ?></th>
            </tr>
        </tfoot>
    </table>
</div>