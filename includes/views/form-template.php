<div class="wrapper-form">
    <h3 class="form-title"><?= __('Formulario Bluecell') ?></h3>
    <form action="<?= site_url() ?>/wp-admin/admin-ajax.php" class="contact-form">
        <div class="group-field">
            <label class="group-field__label"><?= __('Nombre') ?></label>
            <input name="name" class="group-field__field" type="text">
        </div>
        <div class="group-field">
            <label class="group-field__label"><?= __('Email') ?></label>
            <input name="email" class="group-field__field" type="email">
        </div>
        <div class="group-field">
            <label class="group-field__label"><?= __('Tel&eacute;fono') ?></label>
            <input name="phone" class="group-field__field" type="text">
        </div>
        <div class="group-field">
            <label class="group-field__label"><?= __('Mensaje') ?></label>
            <textarea class="group-field__textarea" name="message"></textarea>
        </div>
        <div class="group-field">
            <label class="group-field__label"><?= __('Asunto') ?></label>
            <input name="subject" class="group-field__field" type="text">
        </div>
        <div class="group-field">
            <input name="policies_conditions" id="policies_conditions" class="group-field__field" type="checkbox">
            <label class="group-field__label" for="policies_conditions"><?= __('Acepto las Pol&iacute;ticas y Condiciones') ?></label>
        </div>
        <input type="hidden" name="action" value="ajax_bluecell_form">
        <?php wp_nonce_field('wp_nonce_form', 'bluecell_form_nonce'); ?>
        <div class="group-field">
            <button type="submit" class="group-field__button"><?= __('Enviar') ?></button>
        </div>
        <div id="messages"></div>
    </form>
</div>